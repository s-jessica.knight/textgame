
package TextGame;

import java.util.Scanner;
import java.util.Set;


public class UI {
    Player player;
    Scanner scanner;
    Controller control;
    
    public UI(Player player) {
        this.player = player;
        scanner = new Scanner(System.in);
        control = new Controller(player);
    }
    
    public void readInput(String input) {
        String output = "";
        if (input.equals("S") || input.equals("N") || input.equals("E") || input.equals("W")) {
            output += control.requestMove(input);
        } else if (input.startsWith("TAKE")) { 
            output += control.requestTake(input); 
        } else if (input.equals("INVENTORY")) {
            output += control.requestInventory();
        } else if (input.startsWith("USE")) {
            output += control.requestUse(input); 
        } else if (input.contains("BACK") || input.equals("B")) {
            output += control.requestBack();
        } else if (input.equals("SAVE")) {
            output += control.requestSave();
        }
        if (!output.isEmpty()) {
            System.out.println(output + "\n");
        } 
    }
    
    public void printIntro(String intro) {
        System.out.println("\n" + intro);
    }
    
    public void printCommands(String commands) {
        System.out.println(commands);
    }
    
    public void takeInput() {
        String line = "";
        while (!line.equals("QUIT") && !line.equals("Q") && !player.caughtEmAll()) {
            line = scanner.nextLine().toUpperCase();
            readInput(line);
        }
        if (player.caughtEmAll()) {
            System.out.println("\nYOU COLLECTED ALL THE ITEMS!");
        }
    }
    
    public String chooseGame() {
        System.out.println("ENTER 1 FOR NEW GAME. ENTER 2 FOR SAVED GAME.");
        String choice = scanner.nextLine();
        return choice;
    }
    
    public void intro() {
        System.out.println("YOU ARE IN: " + player.getLocName());
        System.out.println(player.getLocDes());
    }
    
    public void launch() {
        intro();
        takeInput();
    }
    
    private String gameChoice() {
        System.out.println("ENTER 1 FOR NEW GAME. ENTER 2 FOR SAVED GAME.");
        return scanner.nextLine();
    }
}
