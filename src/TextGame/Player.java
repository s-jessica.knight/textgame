
package TextGame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Stack;


public class Player {
    private Map<String, Item> inventory;
    private Stack<Room> route;
    private Room location;
    Set<String> solved;
    
    public Player(Room startLoc) {
        location = startLoc;
        inventory = new HashMap<String, Item>();
        route = new Stack<Room>();
        route.add(startLoc);
        solved = new HashSet<>();
    }
    
    public Player() {
        inventory = new HashMap<String, Item>();
        route = new Stack<Room>();
        solved = new HashSet<>();

    }
    
    public void setStartRoom(Room startLoc) {
        this.location = startLoc;
        route.add(startLoc);
    }
    
    public boolean addInv(Item item) {
        int size = inventory.size();
        inventory.put(item.getName(), item);
        int newSize = inventory.size();
        return newSize > size;
    }
    
    public String checkInv() {
        String inv = "Inventory:\n";
        for (String key : inventory.keySet()) {
            inv += key + "\n";
        }
        return inv;
    }
    
    public String getLocDes() {
        return location.getDes();
    }
    
    public String getLocName() {
        return location.getName();
    }
    
    public boolean takeItem(String input) {
        if (nearItem(input)) {
            return addInv(getNearbyItem(input));
        }
        return false;
    }
    
    public String move(String dir) {        
        boolean roomThere = location.isAdj(dir);
        boolean notBlocked = !location.isDirBlocked(dir);
        
        if (roomThere && notBlocked) {
            this.location = location.getAdj(dir);
            route.push(location);
            return "move successful";
        } else if (!roomThere) {
            return "No where to go in that direction.";
        } else {
            return "That direction is blocked by " + location.dirBlockedBy(dir);
        }
    }
    
    public String getPathStr() {
        String s = "Path:\n";
        for (Room room : route) {
            s += room.getName() + "\n";
        }
        return s;
    }
    
    public boolean goBack() {
        if (route.size() < 2) {
            return false;
        } else {
            route.pop();
            location = route.peek();
            return true;
        }
    }
    
    public boolean nearItem(String name) {
        return location.containsItem(name);
    }
    
    public Item getNearbyItem(String name) {
        return location.getItem(this, name);
    }
    
    public boolean caughtEmAll() {
        return inventory.size() == Item.totalItems;
    }

    
    public Set<String> useItem(String itemName) {
        int before = solved.size();
        Set<String> obsSolved = new HashSet<>();
        if (inventory.containsKey(itemName)) {
            obsSolved = location.solveObs(itemName);
        }
        solved.addAll(obsSolved);
        int after = solved.size();
        
        return obsSolved;
    }
    
    public void addSolved(String solvedObs) {
        solved.add(solvedObs);
    }

    public void save() {
        try {
            PrintStream output = new PrintStream(new File("src/savedGameFiles/savedStartRoom.txt"));
            output.println(location.getName());
            
            PrintStream out = new PrintStream(new File("src/savedGameFiles/solvedObstacles.txt"));
            String solvedNames = "";
            for (String s : solved) {
                solvedNames += s + "\n";
            }
            
            out.print(solvedNames);
            
            PrintStream o = new PrintStream(new File("src/savedGameFiles/savedInventory.txt"));
            String inventory = "";
            for (String key : this.inventory.keySet()) {
                inventory += key + "\n";
            }
            o.print(inventory);
        } catch (FileNotFoundException e) {
            
        }
    }
    
}
