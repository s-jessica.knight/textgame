
package TextGame;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Room {
    private String name;
    private Map<String, Room> adjRooms; // key is direction from this room
    private Map<String, Obstacle> obstacles; // key is blocked direction
    private Map<String, Item> items;
    
    
    public Room(String name) {
        this.name = name;
        adjRooms = new HashMap<>();
        obstacles = new HashMap<>();
        items = new HashMap<>();
    }
    
    public String getName() {
        return name;
    }
    
    public String getDes() {
        
        if (items.keySet().isEmpty()) {
            return "";
        }
        
        String des = "ITEMS HERE:";
        for (String key : items.keySet()) {
            des += " " + key + ",";
        }
        return des.substring(0, des.length()-1);
    }
    
    public boolean isAdj(String dir) {
        return adjRooms.containsKey(dir);
    }
    
    public Room getAdj(String dir) {
        return adjRooms.get(dir);
    }
    
    public void addAdj(String dir, Room other) {
        adjRooms.put(dir, other);
        
        if (dir.equals("N")) {
            other.adjRooms.put("S", this);
        } else if (dir.equals("S")) {
            other.adjRooms.put("N", this);
        } else if (dir.equals("E")) {
            other.adjRooms.put("W", this);
        } else if (dir.equals("W")) {
            other.adjRooms.put("E", this);
        } 
    }
    
    public boolean isDirBlocked(String dir) {
        return obstacles.containsKey(dir) && !obstacles.get(dir).isSolved();
    }
    
    public String dirBlockedBy(String dir) {
        return obstacles.get(dir).getName();
    }
    
    public void setObs(String direction, Obstacle obstacle) {
        obstacles.put(direction, obstacle);
    }
    
    public void addItem(Item item) {
        items.put(item.getName(), item);
    }
    
    public void removeItem(String name) {
        items.remove(name);
    }
    
    public boolean containsItem(String name) {
        return items.containsKey(name);
    }
    
    
    public Item getItem(Player player, String name) {
        if (player.getLocName().equals(this.name)) {
            Item item = items.get(name);
            items.remove(name);
            return item;
        } else {
            return null;
        }  
    }
       
    public Set<String> solveObs(String itemName) {
        Set<String> solved = new HashSet<>();
        for (String key : obstacles.keySet()) { // would it be better to iterate through the values instead of keys?
            if(obstacles.get(key).solveWith(itemName)) {
                solved.add(obstacles.get(key).getName());
            }
        }
        return solved;
    }
}
