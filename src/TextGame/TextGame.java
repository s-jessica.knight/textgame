
package TextGame;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;


public class TextGame {
    
    Map<String, Room> rooms;
    FileIO fr;
    Player player;
    
    public TextGame() {
        
    }

    public void setUpRooms() {
        
        player = new Player();
        UI ui = new UI(player);
        String gameChoice = ui.chooseGame();
        fr = new FileIO(gameChoice);
        rooms = fr.loadRooms();
        fr.setRoomDir(rooms);
        fr.createItems(player, rooms);
        fr.createObstacles(player, rooms);
        Room start = fr.getStartRoom(rooms);
        
        player.setStartRoom(start);
        ui.printIntro(fr.getIntro());
        ui.printCommands(fr.getCommands());
        ui.launch();
    }
    
    public static void main(String[] args) {
        TextGame ta = new TextGame();
        ta.setUpRooms();
        
    }
    
}
