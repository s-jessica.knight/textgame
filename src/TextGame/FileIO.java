
package TextGame;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


public class FileIO {
    String gameChoice;
    
    public FileIO(String gameChoice) {
        this.gameChoice = gameChoice;
    }
    
    public Map<String, Room> loadRooms() {
        Map<String, Room> rooms = new HashMap<>();
        try {
            Scanner input = new Scanner(new File("src/gameFiles/Rooms.txt"));
            String line;
            while (input.hasNextLine()) {
                line = input.nextLine();
                rooms.put(line, new Room(line));
            }
            input.close();
        } catch (FileNotFoundException e) {
            System.out.println("Rooms.txt file not found.");
        }
        
        return rooms;
    }
    
    public void setRoomDir(Map<String, Room> rooms) {
        try {
            Scanner scanner = new Scanner(new File("src/gameFiles/RoomDir.txt"));
            String line;
            while (scanner.hasNextLine()) {  
                line = scanner.nextLine();
                String[] strings = line.split("-");
                Room firstRoom = rooms.get(strings[0]);
                String dir = strings[1];
                Room secondRoom = rooms.get(strings[2]);
                if (firstRoom != null && secondRoom != null) {
                    secondRoom.addAdj(dir, firstRoom);
                } else {
                    System.out.println("0: "+strings[0]+"1: "+strings[1]+"2: "+strings[2]);
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("RoomDir.txt file not found.");
        }  
    }
    
    public Room getStartRoom(Map<String, Room> rooms) {
        Room startRoom = null;
        try {
            Scanner scanner;
            if (gameChoice.equals("2")) {
                scanner = new Scanner(new File("src/savedGameFiles/savedStartRoom.txt"));
            } else {
                scanner = new Scanner(new File("src/gameFiles/startRoom.txt"));
            }
            
            String line = "";
            if (scanner.hasNextLine()) {
                line = scanner.nextLine();
            }  
            startRoom = rooms.get(line);
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("startRoom.txt file not found.");
        }
        return startRoom;
    }
    
    
    public Set<String> getSavedItemNames() {
        Set<String> savedItems = new HashSet<>();
        if (gameChoice.equals("2")) {
            try {
                Scanner scanner = new Scanner(new File("src/savedGameFiles/savedInventory.txt"));
                while (scanner.hasNextLine()) {
                    savedItems.add(scanner.nextLine().trim());
                }
                scanner.close();
            } catch (FileNotFoundException e) {
                System.out.println("savedInventory.txt file not found.");
            }
        }
        return savedItems;
    }
    
    public void createItems(Player player, Map<String, Room> rooms) {
        Set<String> savedItems = getSavedItemNames();
        
        try {
            Scanner scanner = new Scanner(new File("src/gameFiles/items.txt"));
            while (scanner.hasNextLine()) {
                String[] arr = scanner.nextLine().split("-");
                
                Item item = new Item(arr[0]);
                
                Room room = rooms.get(arr[1]);
                if (gameChoice.equals("1") || !savedItems.contains(item.getName())) {
                    room.addItem(item);
                } else {
                    player.addInv(item);
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("items.txt file not found.");
        } 
    }
    
    public Set<String> getSolvedObsNames() {
        Set<String> solved = new HashSet<>();
        if (gameChoice.equals("2")) {
            try {
                Scanner scanner = new Scanner(new File("src/savedGameFiles/solvedObstacles.txt"));
                while (scanner.hasNextLine()) {
                    solved.add(scanner.nextLine());
                }
                scanner.close();
            } catch (FileNotFoundException e) {
                System.out.println("solvedObstacles.txt file not found.");
            }
        }
        return solved;
    }
    
    public void createObstacles(Player player, Map<String, Room> rooms) {
        Set<String> solved = getSolvedObsNames();
        
        try {
            Scanner scanner = new Scanner(new File("src/gameFiles/obstacles.txt"));
            while (scanner.hasNextLine()) {
                String[] arr = scanner.nextLine().split("-");
                String name = arr[0];
                String place = arr[1];
                String direction = arr[2];
                String key = arr[3];
                Obstacle ob = new Obstacle(name, key);
                Room room = rooms.get(place);
                if (gameChoice.equals("1") || !solved.contains(name)) {
                    room.setObs(direction, ob);
                } else {
                    player.addSolved(name);
                }
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("obstacles.txt file not found.");
        }
    }
    
    public String getIntro() {
        String intro = "";
        if (gameChoice.equals("2")) {
            return "Welcome back.";
        }
        try {
            Scanner scanner = new Scanner(new File("src/gameFiles/Introduction.txt"));
            while (scanner.hasNextLine()) {
                intro += scanner.nextLine() + "\n";
            }
        } catch (FileNotFoundException e) {
            System.out.println("Introduction.txt file not found.");
        }
        return intro;
    }
    
    public String getCommands() {
        String commands = "";
        
        try {
            Scanner scanner = new Scanner(new File("src/gameFiles/Commands.txt"));
            while (scanner.hasNextLine()) {
                commands += scanner.nextLine() + "\n";
            }
        } catch (FileNotFoundException e) {
            System.out.println("Commands.txt file not found.");
        }
        return commands;
    }
}
