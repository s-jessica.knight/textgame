
package TextGame;

import java.util.Set;


public class Controller {
    Player player;
    
    public Controller(Player player) {
        this.player = player;
    }
    
    public String requestMove(String input) {
        String out = "";
        String s = player.move(input);
        if (s.equals("move successful")) {
            System.out.println("CURRENT LOCATION: " + player.getLocName());
            String description = player.getLocDes();
            if (!description.equals("")) {
                out += description;
            }
        } else {
            out += s;
        }
        return out;
    }
    
    public String requestBack() {
        String out = "";
        boolean possible = player.goBack();
        if (!possible) {
            out += "NOT POSSIBLE";
        } else {
            out += "CURRENT LOCATION: " + player.getLocName();
            String description = player.getLocDes();
            if (!description.equals("")) {
                out += "\n" + description;
            }
        }
        return out;
    }
    
    public String requestSave() {
        player.save();
        return "GAME SAVED";
    }
    
    public String requestInventory() {
        return "\n" + player.checkInv();
    }
    
    public String requestTake(String input) {
        input = input.substring(4);
        input = input.trim();
        if (player.takeItem(input)) {
            return "\n" + input + " ADDED TO INVENTORY";
        } else {
            return "NOPE";
        }
    }
    
    public String requestUse(String input) {
        input = input.substring(3);
        input = input.trim();
        Set<String> obsSolved = player.useItem(input);
        boolean worked = obsSolved.size() >= 1;
        String output = "";
        if (worked) {
            output = "THAT WORKED!";
        } else {
            output = "THAT DIDN'T WORK.";
        }
        return output;
    }
    
}
