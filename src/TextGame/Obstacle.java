
package TextGame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;


public class Obstacle {
    private boolean solved;
    private String obstacleName;
    private String keyName;
    
    public Obstacle(String name, String keyName) {
        this.obstacleName = name;
        this.keyName = keyName;
        
    }
    
    public String getName() {
        return obstacleName;
    }

    public boolean isSolved() {
        return solved;
    }
    
    public boolean solveWith(String itemName) {
        solved = keyName.equals(itemName);
        return solved;
    }
    
}
