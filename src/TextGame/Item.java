
package TextGame;

public class Item {
    public static int totalItems = 0;
    private String name;
    
    public Item(String name) {
        this.name = name;
        totalItems++;
    }
    
    public String getName() {
        return name;
    }  
}
